package org.example.sortingAppProjectTest;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.example.sortingAppProject.Sorting;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;



@RunWith(Parameterized.class)
public class SortingParametrizedTest {
    private final String[] inputArrayToSort;
    private final String expectedSortedOutput;

    public SortingParametrizedTest(String[] inputArrayToSort, String expectedSortedOutput) {
        this.inputArrayToSort = inputArrayToSort;
        this.expectedSortedOutput = expectedSortedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, "There was no input\n"},
                {new String[]{"1"}, "1\n"},
                {new String[]{"-1", "8", "1", "-1", "22", "0", "-7", "4", "15", "12"}, "-7\n-1\n-1\n0\n1\n4\n8\n12\n15\n22\n"},
                {new String[0], "There was no input\n"}
        });
    }

    @Test
    public void testingWithParameters() {
        String actualOutput = getProgramOutput(inputArrayToSort);
        assertEquals(expectedSortedOutput, actualOutput);
    }


    public String getProgramOutput(String[] input) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        PrintStream originalOutput = System.out;

        try (PrintStream ignored = printStream) {
            System.setOut(printStream);
            Sorting.main(input);
            return outputStream.toString();
        } finally {
            System.setOut(originalOutput);
        }
    }
}
